import open3d as o3d
import os
import numpy as np
import struct

cloud_path = r'D:\ONCE\3D_Lidars\train_lidar\data\000076\lidar_roof'
cloud_list = os.listdir(cloud_path)
cloud_list.sort()
for i in range(0, len(cloud_list), 1):
    print(cloud_list[i])
    # if not cloud_list[i] == '0001_108.pcd':
    #     continue
    # print(cloud_list[i])
    _cloud_path = os.path.join(cloud_path, cloud_list[i])
    if _cloud_path[-4:] == '.bin':
        pcd_np = np.fromfile(_cloud_path, dtype=np.float32).reshape(-1, 4)
        pcd = o3d.geometry.PointCloud()
        pcd.points = o3d.utility.Vector3dVector(pcd_np[:, :3])
    elif cloud_path[-4:] == '.pcd' or '.ply':
        pcd = o3d.io.read_point_cloud(_cloud_path, remove_nan_points=True)
    temp_points = np.asarray(pcd.points)
    print(temp_points.shape)
    print(np.max(temp_points[:,0]),np.min(temp_points[:,0]),np.max(temp_points[:,1]),np.min(temp_points[:,1]),\
        np.max(temp_points[:,2]),np.min(temp_points[:,2]))
    vis = o3d.visualization.Visualizer()
    vis.create_window()
    vis.add_geometry(pcd)
    vis.get_render_option().point_size = 2
    vis.run()
    vis.destroy_window()