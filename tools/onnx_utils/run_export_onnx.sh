CUDA_VISIBLE_DEVICES=2 python export_onnx.py --cfg_file /home/hning/lidar/hn_openpcdet/tools/cfgs/zhisuo_models/centerpoint.yaml \
--onnx_path /home/hning/lidar/hn_openpcdet/output/onnx_files \
--ckpt /home/hning/lidar/hn_openpcdet/output/zhisuo_models/centerpoint/default/ckpt/checkpoint_epoch_80.pth \
--head_type center \
--need_check True